import React from 'react';
import {View, ScrollView, Styles, Text, StyleSheet, Image} from 'react-native';

const Caption = (props) => {
    return (
        <View style={style.captionContainer}>
            <View style={style.caption}>
                <Text>liked by <Text style={{fontWeight : 'bold'}}>harisrohan</Text> and <Text style={{fontWeight : 'bold'}}>{props.likes} others</Text></Text>
                <Text style={{fontWeight : 'bold',}}>harisrohan <Text style={{fontWeight : 'normal'}}> Your caption goes here...</Text></Text>
            </View>
        </View>
    );

};

const style = StyleSheet.create({
    captionContainer: {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'flex-start',
        paddingLeft : 10,
        height : 50,
        backgroundColor : 'white',
    },
    caption: {
        display : 'flex',
        position : 'relative',
    },
});

export default Caption;