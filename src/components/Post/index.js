import React from 'react';
import {View, ScrollView, Styles, Text, StyleSheet, Image} from 'react-native';
import Post from '../../assets/Post.jpg';

const Content = () => {
    return (
        <View style={style.container}>
            <Image source={Post} style={{resizeMode : 'center', width : 350}}/>
        </View>
    );

};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        alignItems : 'center',
        justifyContent : 'center',
        height : 400,
        backgroundColor : 'pink',
    },
});

export default Content;