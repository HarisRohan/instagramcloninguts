import React from 'react';
import {View, Styles, Text, StyleSheet, Image} from 'react-native';
import Instagram from '../../assets/Instagram.png';
import Notification from '../../assets/Notification.png';
import Message from '../../assets/Message.png';

const Header = () => {
    return (
        <View style={style.container}>
            <Image source={Instagram} style={{flex : 0, resizeMode : "center", width : 110, height : 35, position : 'absolute', left : 15}}/>
            <View style={style.notification}>
                <Image source={Notification} style={{flex : 2, resizeMode : "center", width : 25, height : 30, marginRight : 30}}/>
                <Image source={Message} style={{flex : 2, resizeMode : "center", width : 25, height : 30,}}/>
            </View>
        </View>
    )
};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        borderWidth : 0,
        borderBottomWidth : 0.8,
        borderBottomColor : '#d4d4d4',
        flexDirection : 'row',
        alignItems : 'center',
        height : 42,
        padding : 30,
        shadowColor : 'grey',
        backgroundColor : 'white',
        position : 'relative',
    },
    notification: {
        display : 'flex',
        flex : 1,
        flexDirection : 'row',
        position : 'absolute',
        right : 20,
    },
});

export default Header;