import React from 'react';
import {View, TouchableOpacity, Styles, Text, StyleSheet, Image} from 'react-native';
import Post from '../../assets/Post.jpg';
import Option from '../../assets/Option.png';


const User = (props) => {
    return (
            <View style={style.container}>
                <View style={{display : 'flex', flexDirection : 'row', fontSize : 17, alignItems : 'center', position : 'absolute', left : 0}}>
                    <TouchableOpacity onPress={null}>
                        <Image source={Post} style={{resizeMode : 'contain', width : 40, height : 40, margin : 10, borderRadius : 200}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize : 15, fontWeight : 'bold'}}>harisrohan</Text>
                </View>
                <Image source={Option} style={{display : 'flex', flex : 0, resizeMode : 'contain', height : 15, position : 'absolute', right : 0}}/>
            </View>
    );

};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        borderWidth : 0,
        flexDirection : 'row',
        alignItems : 'center',
        // justifyContent : 'space-evenly',
        height : 60,
        // flexWrap : 'wrap',
        // position : 'relative',

    },
});

export default User;