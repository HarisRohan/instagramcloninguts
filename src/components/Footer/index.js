import React from 'react';
import {View, ScrollView, Styles, Text, StyleSheet, Image} from 'react-native';
import Home from '../../assets/Home.png';
import Search from '../../assets/Search.png';
import AddPost from '../../assets/Add_Post.png';
import Shop from '../../assets/Shop.png';
import Post from '../../assets/Post.jpg';

const Footer = () => {
    return (
        <View style={style.container}>
            <Image source={Home} style={{resizeMode : 'center', width : 25, height : 30}}/>
            <Image source={Search} style={{resizeMode : 'center', width : 25, height : 30}}/>
            <Image source={AddPost} style={{resizeMode : 'center', width : 25, height : 30}}/>
            <Image source={Shop} style={{resizeMode : 'center', width : 25, height : 30}}/>
            <Image source={Post} style={{resizeMode : 'center', width : 25, height : 30, borderRadius : 200}}/>
        </View>
    );

};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center',
        bottom : 0,
        justifyContent : 'space-evenly',
        height : 60,
        width : '100%',
        backgroundColor : 'white',
        position : 'relative',
    },
});

export default Footer;