import React from 'react';
import {ScrollView, View, Styles, Text, StyleSheet, Image} from 'react-native';
import addStory from '../../assets/Add_Story.png';
import Stories from '../../assets/Stories.png';
import Post from '../../assets/Post.jpg';

const Story = () => {
    return (
        <ScrollView horizontal={true}>
            <View style={style.container}>
                <View style={style.story}>
                    <View style={{alignItems : 'center'}}>
                        <Image source={Post} style={style.img}/>
                        <Image source={addStory} style={style.plus}/>
                    </View>
                    <Text style={style.text}>Your Story</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
                <View style={style.story}>
                    <Image source={Stories} style={style.img}/>
                    <Text style={style.text}>username</Text>
                </View>
            </View>
        </ScrollView>
    );

};

const style = StyleSheet.create({
    container: {
        display : 'flex',
        borderWidth : 0,
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'center',
        height : 93,
        padding : 10,
    },
    story : {
        display : 'flex',
        flexDirection : 'column',
        alignItems : 'center',
        fontSize : 17,
    },
    text: {
        fontSize : 12,
    },
    plus: {
        width : 22,
        height : 22,
        position : 'absolute',
        bottom : 2,
        right : 8,
        alignContent : 'center',
    },
    img: {
        flex : 0,
        resizeMode : 'contain',
        borderRadius : 200,
        width : 120,
        height : 65,
        marginTop : 5,
        marginBottom : 5,
        marginRight : -20,
        marginLeft : -20,
    },
});

export default Story;