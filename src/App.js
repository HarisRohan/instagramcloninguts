import React , {useState} from 'react';
import {ScrollView, View} from "react-native";
import Header from './components/Header';
import Story from './components/Story';
import User from './components/User';
import Post from './components/Post';
import Action from './components/Action';
import Caption from './components/Caption';
import Suggestion from './components/Suggestion';
import Footer from './components/Footer';

const InstagramCloning = () => {
  const [love, taplove] = useState(0);
    return (
      <View>
      <Header/>
        <ScrollView>
          <Story/>
          <User/>
          <Post/>
          <Action tap={() => taplove(love+1)} />
          <Caption likes={love} reset={() => taplove(0)} />
          <Suggestion/>
          <User/>
          <Post/>
          <Action tap={() => taplove(love+1)} />
          <Caption likes={love} reset={() => taplove(0)} />
          <User/>
          <Post/>
          <Action tap={() => taplove(love+1)} />
          <Caption likes={love} reset={() => taplove(0)} />
        </ScrollView>
      </View>
    );
};


export default InstagramCloning;
